// authors: Christos Bakalis (christos.bakalis@cern.ch)
//          Dimitrios Matakias (dimitrios.matakias@cern.ch)

// g++ -g -Wall -I ../include/ analog.cpp socket_listen.cpp -o daq_server

#ifndef analog
#define analog

#include <vector>
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <fstream>
#include "constants.h"

struct packet_header //boardId:channel:flag:threshold:type
{
    unsigned char eventCnt1 : 8; // MSB
    unsigned char eventCnt0 : 8; // LSB
    unsigned char boardId   : 8;
    unsigned char threshold : 1;
    unsigned char flag      : 1;
    unsigned char channel   : 6;
    unsigned char type0     : 8;
    unsigned char type1     : 8;
    // unsigned char type2     : 8;
    // unsigned char type3     : 8;
    //int32_t      type      : 32;     
};

enum class packetType 
{
    type_pdo, 
    type_tdo,
    type_unknown
};

class Analog
{
    public:
        Analog(std::vector<unsigned char>, std::ofstream&, std::ofstream&);
        ~Analog();
        packet_header header;
        packetType type;
        unsigned char trailer[4];
        unsigned char *packet_charArray;
        std::vector<uint16_t> xadcSamples;
 
    private:
        std::vector<unsigned char> dissect_buffer(std::vector<unsigned char>);
        std::vector<uint16_t> parse_xadcSamples(std::vector<unsigned char>);
        void print_data(std::vector<uint16_t>, struct packet_header, std::ofstream&);
};

#endif
