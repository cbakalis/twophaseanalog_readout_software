// authors: Christos Bakalis (christos.bakalis@cern.ch)
//          Dimitrios Matakias (dimitrios.matakias@cern.ch)

#include <iostream>
#include <fstream>
#include <vector>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/udp.h>
#include <netinet/ip.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <regex>
#include <iterator>
#include "analog.h"
#include <signal.h>
#include <unistd.h>
//#include "constants.h"

    // global variables
    int socket_in = 0;
    struct sockaddr_in si_me, si_other;
    char *receive_buffer;
    ssize_t receive_buffer_len = 0;
    std::ofstream pdoFile;
    std::ofstream tdoFile;

// for intercept
void my_handler(int s)
{
    printf("Caught signal %d\n",s);
    pdoFile.close();
    tdoFile.close();
    close(socket_in);
    exit(1); 
}

void init_udp_input_socket()
{
    memset((char *) &si_me, 0, sizeof(si_me));

    si_me.sin_family = AF_INET;
    si_me.sin_port = htons(input_port);
    si_me.sin_addr.s_addr = htonl(INADDR_ANY);

    if( bind(socket_in, (struct sockaddr*)&si_me, sizeof(si_me) ) == -1)
    {
        perror("socket bind error");
        exit(1);
    }

    receive_buffer = new char[max_buffer_len];
}

std::vector<std::string> stolen(std::string& s, const char& c)
{
    std::string buff{""};
    std::vector<std::string> v;
    
    for(auto n:s)
    {
        if(n != c) buff+=n; else
        if(n == c && buff != "") { v.push_back(buff); buff = ""; }
    }
    if(buff != "") v.push_back(buff);
    
    return v;
}

std::vector<std::vector<unsigned char>> parse(std::string file_nameI)
{
    std::vector<std::vector<unsigned char>> buffer;
    std::ifstream inFile;
    const std::string regexFilter = "1a 00 00 00( |([0-9]|[a-f]){2}|\n([0-9]|[a-f]){2})+";
    std::smatch sm;
    std::regex rx = std::regex(regexFilter);

    inFile.open(file_nameI);

    if(!inFile)
    {
        std::cout << "Unable to open file" << std::endl;
        exit(1);
    }

    std::ifstream ifs(file_nameI);
    std::string content( (std::istreambuf_iterator<char>(ifs) ), (std::istreambuf_iterator<char>()    ) );
    auto words_begin = std::sregex_iterator(content.begin(), content.end(), rx);
    auto words_end = std::sregex_iterator();

    for (std::sregex_iterator i = words_begin; i != words_end; ++i)
    {
        std::vector<unsigned char> unsignedCharPacket;
        std::smatch match = *i;                                                 
        std::string match_str = match.str();
        match_str.replace(0, 12, ""); 
        match_str.replace(14, 1, " ");
        //std::cout << match_str << std::endl;

        std::vector<std::string> stringPacket;
        stringPacket = stolen(match_str, ' ');

        for(unsigned int i = 0; i < stringPacket.size(); i++)
        {
            //std::cout << stringPacket[i];
            std::string s = "0x" + stringPacket[i];
            unsigned int x = stoul(s, nullptr, 16);
            unsignedCharPacket.push_back(x);
        }

        buffer.push_back(unsignedCharPacket);
    }

    return buffer;
}

void receive_packet()
{
    socklen_t slen = sizeof(si_other);
    // listen to port
    receive_buffer_len = recvfrom(socket_in, receive_buffer, max_buffer_len, 0, (struct sockaddr *) &si_other, &slen);
    if (receive_buffer_len == -1)
    {
        perror("socket length error");
        exit(1);
    }
}

int main(int argc, char* argv[])
{
    std::string input = "";
    unsigned int nEventsToDo = 1000;

    if ( (argc <= 1) || (argv[argc-1] == NULL) || (argv[argc-1][0] == '-') ) // there is NO input...
    {  
        std::cout << "No argument provided!" << std::endl;
        //return 1;
    }
    else
    {  // there is an input...
        input = argv[argc-1];
    }

    for(int opt = 1; opt < argc; opt++)
    {
        std::string option = argv[opt];
        if(option == "events")
        {
          nEventsToDo = atoi(argv[++opt]); //example: ./daq_server events 1000 (crude...but was in a hurry)
        }
    }

    // intercept signal
   struct sigaction sigIntHandler;

   sigIntHandler.sa_handler = my_handler;
   sigemptyset(&sigIntHandler.sa_mask);
   sigIntHandler.sa_flags = 0;

   sigaction(SIGINT, &sigIntHandler, NULL);



    // std::vector<std::vector<unsigned char>> buffer;
    // std::vector<unsigned char> packet;
    pdoFile.open ("pdo.txt");
    tdoFile.open ("tdo.txt");

    socket_in = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    init_udp_input_socket();

    std::vector<unsigned char> udpBuffer_vec;

    // std::cout << "buffer contents: " << std::endl;

    // parse into buffer
    unsigned int packetNumber = 0;
    while(packetNumber < nEventsToDo)
    {
        receive_packet();
        std::cout << "packet received. packet number: " << packetNumber++ << std::endl;
        std::cout << "limit is: " << nEventsToDo << std::endl;

        for(unsigned int i = 0; i < receive_buffer_len; i++)
        {
            // std::cout << (uint16_t)receive_buffer[i] << " ";
            udpBuffer_vec.push_back((unsigned char)receive_buffer[i]);
        }

        Analog x(udpBuffer_vec, pdoFile, tdoFile);
        udpBuffer_vec.clear();
    }

    // std::cout << "buffer end" << std::endl;
    // buffer = parse("dump3.txt");
    // //std::cout << "size: " << buffer.size() << std::endl;
    // for(unsigned int i = 0; i < buffer.size(); i++)
    // {
    //     packet = buffer[i]; 
    //     Analog x(packet, pdoFile, tdoFile);
    // }

    // close files and socket

    return 0;
}
