// authors: Christos Bakalis (christos.bakalis@cern.ch)
//          Dimitrios Matakias (dimitrios.matakias@cern.ch)

#include "analog.h"

Analog::Analog(std::vector<unsigned char> packet_in, std::ofstream& pdoFile, std::ofstream& tdoFile)
{
    
    packet_charArray = new unsigned char[packet_in.size()];

    for(unsigned int i = 0; i < packet_in.size(); i++)
    {
        packet_charArray[i] = packet_in[i];
        //std::cout << (int)packet_charArray[i] << std::endl;
    }

    memcpy(&header, packet_charArray, sizeof(struct packet_header));
    memcpy(&trailer, packet_charArray + packet_in.size() - sizeof(trailer), sizeof(trailer));

    xadcSamples = parse_xadcSamples(dissect_buffer(packet_in));

    if(header.type0 == pdo_type_int)
    {
        type = packetType::type_pdo;
        print_data(xadcSamples, header, pdoFile);
    }
    else if(header.type0 == tdo_type_int)
    {
        type = packetType::type_tdo;
        print_data(xadcSamples, header, tdoFile);
    }
    else
    {
        type = packetType::type_unknown;
        std::cout << "ERROR: Unknown packet type." << std::endl;
    }
}

Analog::~Analog()
{
    delete[] packet_charArray;
}

// removes header and trailer from the packet
std::vector<unsigned char> Analog::dissect_buffer(std::vector<unsigned char> packet)
{
    std::vector<unsigned char>::const_iterator first = packet.begin() + sizeof(struct packet_header);
    std::vector<unsigned char>::const_iterator last = packet.end() - sizeof(trailer); // account for 4-byte trailer
    std::vector<unsigned char> rawXadcData(first, last);
    
    return rawXadcData;
}

// gets xADC data from the packet
std::vector<uint16_t> Analog::parse_xadcSamples(std::vector<unsigned char> rawXadcIn)
{
    std::vector<uint16_t> xadcSamples; 
    for(unsigned int i = 0; i < rawXadcIn.size(); i+=8)
    {
        unsigned int j = 7;
        for(unsigned int n = 0; n < 5; n++)
        {
            if(n%2 == 0) //nibble&byte
            {
                xadcSamples.push_back((((uint16_t)rawXadcIn[i+j-1]) & 0x0F) << 8 | (uint16_t)rawXadcIn[i+j]);
                j--;
            }
            else //byte&nibble
            {   
                xadcSamples.push_back(((uint16_t)rawXadcIn[i+j-1]) << 4 | ((uint16_t)rawXadcIn[i+j]) >> 4);
                j-=2;
            }
        }
    }

    return xadcSamples;
}

// now averages on the samples
void Analog::print_data(std::vector<uint16_t> xadcSamples, struct packet_header header, std::ofstream& myfile)
{

  uint32_t xadcSamples_sum    = 0;
  uint32_t xadcSamples_inSum  = 0;

  for(unsigned int i = 0; i < xadcSamples.size(); i++)
  {
    if(xadcSamples[i] != 0)
    {
      xadcSamples_sum = xadcSamples[i] + xadcSamples_sum;
      xadcSamples_inSum++;
      std::cout << "sample number: " << i << " = " << xadcSamples[i] << std::hex << std::endl;
    }
  }

  myfile  << (uint16_t)header.boardId
          << " " << ((uint16_t)header.eventCnt1 << 8) + (uint16_t)header.eventCnt0 
          << " " << (uint16_t)header.flag 
          << " " << (uint16_t)header.threshold 
          << " " << (uint16_t)header.channel 
          << " " << (uint16_t)(xadcSamples_sum/xadcSamples_inSum) 
          << std::endl;
  
}
