// authors: Christos Bakalis (christos.bakalis@cern.ch)
//          Dimitrios Matakias (dimitrios.matakias@cern.ch)

#ifndef constants
#define constants

const int input_port = 6272;
const int max_buffer_len = 2000;
const int pdo_type_int = 186;
const int tdo_type_int = 190;

#endif