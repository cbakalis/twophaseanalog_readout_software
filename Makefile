CC=g++
SRC_DIR = src
OBJ_DIR = obj
INC_DIR = include
CFLAGS=-I$(INC_DIR)
SRC_FILES = $(wildcard $(SRC_DIR)/*.cpp)
OBJ_FILES = $(patsubst $(SRC_DIR)/%.cpp,$(OBJ_DIR)/%.o,$(SRC_FILES))
INC_FILES = $(wildcard $(INC_DIR)/*.h)

# %.o: %.cpp $(INC_FILES)
# 	$(CC) -c -o $@ $< $(CFLAGS)

# $(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
# 	$(CC) $(CFLAGS) -Wall -c -o $@ $<


compile: $(SRC_FILES)
	$(CC) -c -o $(CFLAGS)

link: compile
	$(CC) -o $@ $^ $(CFLAGS)

all: link

.PHONY: clean

clean:
	rm -f $(OBJ_DIR)/*.o *~ core $(INC_DIR)/*~ 

