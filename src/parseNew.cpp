#include "Riostream.h"
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"

void parse(uint16_t flag, uint16_t threshold, uint16_t channel, uint16_t adcCount) 
{
//  Read data from an ascii file and create a root file with an histogram and an ntuple.
//   see a variant of this macro in basic2.C
//Author: Rene Brun


// read file $ROOTSYS/tutorials/tree/basic.dat
// this file has 3 columns of float data
   TString dir = gSystem->UnixPathName(__FILE__);
   dir.ReplaceAll("parse.C","");
   dir.ReplaceAll("/./","/");
   ifstream in;
   in.open(Form("%spdo.txt",dir.Data()));

   m_calib_tree   = new TTree("calib", "calib");
   br_flag        = m_calib_tree->Branch("flag", &flag);
   br_threshold   = m_calib_tree->Branch("threshold", &threshold);
   br_channel     = m_calib_tree->Branch("channel", &channel);
   br_adcCount    = m_calib_tree->Branch("adcCount", &adcCount);

   Int_t nlines = 0;
   TFile *f = new TFile("basic.root","RECREATE");
   TH1F *h1 = new TH1F("h1","ADC Counts",100,-4,4);
   TNtuple *ntuple = new TNtuple("ntuple","data from ascii file","flag:threshold:channel:adcCount");

   while (1) {
      in >> flag >> threshold >> channel >> adcCount;
      if (!in.good()) break;
      //if (nlines < 5) printf("x=%8f, y=%8f, z=%8f\n",x,y,z);
      h1->Fill(adcCount);
      ntuple->Fill(flag,threshold,channel, adcCount);
      nlines++;
   }
   printf(" found %d points\n",nlines);

   in.close();

   f->Write();
}